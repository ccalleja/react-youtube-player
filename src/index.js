import _ from 'lodash';
import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import YTSearch from 'youtube-api-search';

//this imports what the export defines in the file
import SearchBar from './components/search_bar';
import VideoList from './components/video_list';
import VideoDetail from './components/video_detail';
//for our files we need to reference the path else it looks in node_modules


const API_KEY = 'AIzaSyALr_LbGGwAyUJhkGXQ39ga__UfGBuBzGU';

//Create component
class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            videos: [],
            selectedVideo: null
        };

        this.videoSearch('surfboards');
    }

    videoSearch (term){
        YTSearch({key: API_KEY, term: term}, (videos) => {
            //this.setState({videos: videos});
            //same as above if key and value var name is the same
            //this.setState({videos});
            this.setState({
                videos:videos,
                selectedVideo:videos[0]
            });
        });
    }

    render() {
        //const is a constant with final value
        const videoSearch = _.debounce((term) => {this.videoSearch(term)}, 300);
        //lodash offers debounce, calling a function once once every x millisenconds
        return (
            <div>
                <SearchBar onSearchTermChange={videoSearch}/>
                <VideoDetail video={this.state.selectedVideo}/>
                <VideoList
                    onVideoSelect={selectedVideo => this.setState({selectedVideo})}
                    videos={this.state.videos}/>
            </div>
        );
    }
}

//Add to page
ReactDOM.render(<App />, document.querySelector('.container'));