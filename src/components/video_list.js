import React from 'react';
import VideoListItem from './video_list_item';

//noneed for state so we can use functional component not class
const VideoList = (props) => {

    const videos = props.videos.map((video)=>{
        return (
            <VideoListItem
                onVideoSelect={props.onVideoSelect}
                key={video.etag}
                video={video}/>
        );
    });

    return (
        //class is reserved for classes in es6 so we use className for css
        <ul className="col-md-4 list-group">
            {videos}
        </ul>
    );
};

export default VideoList;
