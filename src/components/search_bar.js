import React, {Component} from 'react';

/*
 const SearchBar = () => {
 return <input />;
 };
 above same as below

 class SearchBar extends React.Component {
 //extending Component must define render
 render(){
 return <input />;
 }
 }
 */

class SearchBar extends Component {

    constructor(props) {
        super(props);

        //state is only changed like this in constructor
        this.state = {term: ''};
        //out of here we use setState
    }

    /**
     * Extending Component must define render
     * */
    render() {
        //return <input onChange={this.onInputChange}/>;
        return (
            <div className="search-bar">
                <input
                    value={this.state.term} //turn it into a controlled component : value set by state
                    onChange={event => this.onInputChange(event.target.value)}/>
            </div>
        );
    }

    onInputChange(term) {
        this.setState({term});
        this.props.onSearchTermChange(term);
    }
}

export default SearchBar;