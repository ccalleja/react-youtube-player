# Youtube Video Player

Cloned from [ReduxSimpleStarter ](https://github.com/StephenGrider/ReactStarter/releases) following the course [React-Redux](https://www.udemy.com/react-redux/)

A simple application allowing the user to seacrh and play youtube videos

### How to run
```
> npm install
> npm start
```